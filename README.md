**This is an introduction to astrophysical simulations using the Astrophysical Multipurpose Software Environment (AMUSE -> [official GitHub](https://github.com/amusecode/amuse))**

The examples are duplicated as Jupyter notebooks or simple python scripts with minor differences.

- 0intro.py: To start with the basics.
- 1gravity.ipynb/.py: Simple *n*-body simulation of a binary or a multiple system (test the equilateral triangle initial conditions to see a chaotic systems).
- 2hydro.ipynb/.py: Simple Smooth Particles Hydrodynamics (SPH) simulation of a gas cloud in equilibrium.
- 3Couple_gas&stars.ipynb: Simple coupling between a binary or multiple system embedded in a gas cloud.
- 4Cloud_collapse.ipynb/.py: Collapse of a gas cloud which produce proto-stars in the high-density peaks of the cloud (basic simulation of a star forming region).
- help_functions.py: Some useful functions to set simulation logs and plots (allow to go a step further in the way AMUSE works).

**The GDF_model directory adds some scripts to build GDF models :**

- GDF_direct.py: to build a GDF model of 1000 particles using a direct N-body code
- GDF_multi.py: to build a GDF model of 10000 particles using the Multiples module of AMUSE (to process better and faster binaries and multiple systems)
- GDF_functions: functions to build the initial conditions and make the energy logs of the 2 previous scripts

- nbody_precision_test.py: To test the precision of different nbody integrator of AMUSE.

The Gravitationnaly-Driven Fragmentation (GDF) is a method to produce self-consistent fragmented distributions of stars using only N-body dynamics. These models can be used to reproduce the self-similar structure of young clusters at the end of the star formation process. They are particularly suited to study the dynamical processes within these regions since the velocity field is consistently built in the same time than the spatial fragmented distribution, leadind to coherent 6-D phase space configuraions.

More details about the method can be found here:
https://ui.adsabs.harvard.edu/abs/2016MNRAS.459.1213D/abstract
https://ui.adsabs.harvard.edu/abs/2020A%26A...644A.141R/abstract


**To install the AMUSE framework:**

You need AMUSE on your desktop to run the examples. See the following steps and/or visit the official repository on [GitHub](https://github.com/amusecode/amuse).

AMUSE is a collaborative platform which gathers several codes from the community.
It allows to use these codes within one single and pretty simple interface (called the amuse-framework) and also to combine them.  
Most of the codes are gravitationnal n-body dynamics integrators, but there is also hydrodynamics solvers (SPH or grid-based methods), stellar evolution codes and some others.  
The whole thing (including all the modules) is quite big to install, and as it is also an ongoing project, all are not perfectly integrated... So I advise to install the framework first and then some of the codes I use in the examples: ph4, Bhtree, and Fi.  

### How to install AMUSE on your desktop

1. Make sure to have C/C++ and fortran 90 compilers. A recent version of GCC is enough. On Ubuntu/debian system, you can install:
 > sudo apt install build-essential curl g++ gfortran gettext zlib1g-dev

On MacOS, you can use the homebrew or macports package manager (both require the Apple Developer Tools and Xcode to be installed).

2. Make sure to have the following libraries (these are usually already installed on Unix system, but it is safer to have a check):

- HDF (version 1.6.5 - 1.8.x)
- MPI (OpenMPI or MPICH)
- FFTW (version >= 3.0)
- GSL
- CMake (version >= 2.4)
- GMP (version >= 4.2.1)
- MPFR (version >= 2.3.1)
    
On Ubuntu:
 > sudo apt install libhdf5-serial-dev hdf5-tools  
 > sudo apt install libopenmpi-dev openmpi-bin  
 > sudo apt install libgsl-dev  
 > sudo apt install cmake  
 > sudo apt install libfftw3-3 libfftw3-dev  
 > sudo apt install libgmp3-dev  
 > sudo apt install libmpfr6 libmpfr-dev

    
3. Check your Python version. It is better to have python 3, but Python 2.7 should work even if it is deprecated.
I advise to create a Python virtual environment to put the AMUSE software, to avoid mixing up the compilers and the libraries with the different Python version you may have.
 > sudo apt install python3-venv  
 > python3 -m venv amuse-env
  
It creates a directory amuse-env/ where you will have all the Amuse sources. To activate the virtual environment, just do:

 > source amuse-env/bin/activate

It acts like a bashrc script for Python. Then you have a clean Python environment to install amuse and all its dependencies, that you can install by pip. (In case the installation fails or do not work, just delete this folder and you will have no traces left.) 
 
4. Install the following python libraries, using pip:
 
 > pip install setuptools docutils numpy h5py mpi4py pytest matplotlib Cython jupyterlab

5. finally install the amuse framework:
 
 > pip install amuse-framework

I do not recommend to install the whole amuse package (`pip install amuse`), because it will try to install all the codes and some may fails which will cause errors. Instead install manually the codes you need, in this tutorials we use ph4 and Fi:

 > pip install amuse-ph4  
 > pip install amuse-fi

If everything go fine, you should be able to run AMUSE within Python! Launch Python and just do
 
 > from amuse.lab import *

If there are no errors , you are fine !

You can find the official book describing all the possibilities of AMUSE with lots of examples here: [https://iopscience.iop.org/book/978-0-7503-1320-9]
