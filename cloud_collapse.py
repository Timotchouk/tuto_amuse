# coding: utf8
"""
In this demo we aim to roughly reproduce the simulations
in Bate et al. (2003) (http://articles.adsabs.harvard.edu/pdf/2003MNRAS.339..577B).
We set up a molecular cloud and add the particles to the Fi SPH code.
We then evolve this for ~ a free fall time, and form sink particles when
the density reaches a specified value.
"""

import numpy as np
import matplotlib.pyplot as plt

from amuse.ext.molecular_cloud import molecular_cloud

from amuse.community.fi.interface import Fi
from amuse.community.ph4.interface import ph4
from amuse.couple.bridge import Bridge

from amuse.units import units, nbody_system, constants
from amuse.support.console import set_printing_strategy
set_printing_strategy("custom", preferred_units=[units.MSun, units.parsec, units.Myr],
                      precision=4, prefix="", separator="[", suffix="]")

from help_functions import *

def accrete_gas(sink, gas, accretion_radius=150 | units.AU):
    accreted_gas = gas[(gas.position - sink.position).lengths() < accretion_radius]
    return accreted_gas

def evolve_model(system, sph, gravity, time_end):
    nb_sink = 0
    time = system.model_time
    while time <= (time_end - 0.5 * system.timestep):
        time += system.timestep
        system.evolve_model(time)

        # If there is high-density gas, form a sink from the densest particle,
        # the following condition stop the computation and we create a sink particle
        density_limit_detection = sph.stopping_conditions.density_limit_detection
        density_limit_detection.enable()
        while density_limit_detection.is_set():
            #             print("max density: {4.3e}".format(max(sph.gas_particles.density).value_in(units.g/units.cm**3)))
            nb_sink = nb_sink+1
            # Identify the sink
            sink_core = sph.gas_particles[sph.gas_particles.density == max(sph.gas_particles.density)][0]

            # Create it
            sink = Particle()
            sink.position = sink_core.position
            sink.velocity = sink_core.velocity

            #Accrete all gas around in a given radius
            accretion_radius=150 | units.AU
            accreted_gas = accrete_gas(sink, sph.gas_particles)

            #Update properties of the sink
            sink.position = accreted_gas.center_of_mass()
            sink.velocity = accreted_gas.center_of_mass_velocity()
            sink.mass = accreted_gas.total_mass()

            #Remove gas_particles and add new born star
            sph.gas_particles.remove_particles(accreted_gas)
            gravity.particles.add_particle(sink)
            print("Sink created at t=%s" % time.in_(units.Myr))
            #             print("max density: {4.3e}".format(max(sph.gas_particles.density).value_in(units.g/units.cm**3)))

            #Contine evolving
            sph.evolve_model(time)

    # Accrete matter onto the sink particles
    for sink in gravity.particles:
        accreted_gas = accrete_gas(sink, sph.gas_particles)
        if not accreted_gas.is_empty:
            agm = accreted_gas.mass
            mwp = sink.position * sink.mass
            mwv = sink.velocity * sink.mass
            for i in range(3):
                mwp[i] += (agm * accreted_gas.position[i]).sum()
                mwv[i] += (agm * accreted_gas.velocity[i]).sum()
            mass = sink.mass + accreted_gas.total_mass()
            sink.position = mwp / mass
            sink.velocity = mwv / mass
            sph.gas_particles.remove_particles(accreted_gas)
    if nb_sink:
        print("%i sinks formed in this step. Sinks are on average %s MSun"
              % (nb_sink, gravity.particles.mass.mean().value_in(units.MSun)))


if __name__ == "__main__":

    Ngas = 1000
    Mcloud = 50. | units.MSun
    Rcloud = 0.375/2 | units.parsec
    converter = nbody_system.nbody_to_si(Mcloud, Rcloud)
    gas = molecular_cloud(targetN=Ngas,
                          convert_nbody=converter,
                          seed=12122019,
                          ethep_ratio=0.01,
                          ekep_ratio=1.,
                          power=-4).result

    rho_cloud = 3*Mcloud/(4*np.pi*Rcloud**3)
    mu = 2.33 | units.amu
    print("Cloud Mass = {:02.1e}[MSun]".format(Mcloud.value_in(units.MSun)))
    print("Mean density = {:f}[nH.cm^-3]".format((rho_cloud/mu).value_in(units.cm ** -3)))

    tff = np.sqrt(3. * np.pi / (32. * constants.G * rho_cloud))
    print("Free-fall time = ", tff)

    sph = Fi(converter, mode="openmp")
    sph.gas_particles.add_particles(gas)

    sph.parameters.gamma = 1.  # default value: 1.6666667
    sph.parameters.isothermal_flag = True
    sph.parameters.integrate_entropy_flag = False

    sph.parameters.epsilon_squared = (0.05 | units.parsec)**2
    sph.parameters.eps_is_h_flag = True

    sph.parameters.timestep = tff/1000

    sph.parameters.stopping_condition_maximum_density = 1e-13 | units.g * units.cm**-3
    density_limit_detection = sph.stopping_conditions.density_limit_detection
    density_limit_detection.enable()

    initial_stars = Particles(2)
    initial_stars.position = [[0.25, 0.4, 0], [-0.25, -0.4, 0]] | units.parsec
    initial_stars.velocity = [[-0.8, -0.5, 1], [0.8, 0.5, -1]] | units.parsec * units.Myr**-1
    initial_stars.mass = 0.5 | units.MSun

    gravity = ph4(converter)
    gravity.parameters.epsilon_squared = (0.05 | units.parsec)**2
    gravity.parameters.force_sync = 1
    gravity.particles.add_particles(initial_stars)

    system = Bridge()
    system.timestep = 2*sph.parameters.timestep
    system.add_system(sph, (gravity,))
    system.add_system(gravity, (sph,))

    time = 0 | units.Myr
    snap_dt = 0.1*tff
    t_end = 0.5*tff
    while time < t_end:
        time += snap_dt
        print("Evolve to time=", time.value_in(units.Myr))
        evolve_model(system, sph, gravity, time)

        view=[-0.5, 0.5, -0.5, 0.5, 0, 0] | units.pc
        grid_size=(128, 128, 1)
        rho, v, cells_length = sph_to_grid(sph, view, grid_size)
        rho = (rho.sum(axis=2)/mu).value_in(units.cm**-3)
        # rho = (rho.sum(axis=2) * cells_length[2]).value_in(units.amu / units.cm ** 2)
        ax = plot_hydro(rho, view[:4], time=time, stars=gravity.particles)

    sph.stop()


