# coding: utf8
"""
Sets useful function
"""
import numpy as np
import matplotlib.pyplot as plt

from amuse.datamodel import Particles, Particle, new_regular_grid
from amuse.ext.orbital_elements import new_binary_from_orbital_elements
from amuse.io import write_set_to_file, read_set_from_file

from amuse.units import units, nbody_system, constants
from amuse.support.console import set_printing_strategy
set_printing_strategy("custom", preferred_units=[units.MSun, units.parsec, units.Myr],
                      precision=4, prefix="", separator="[", suffix="]")


#####################            Energy               ##########################

def log_nbody(stars, converter, time=0|units.Myr ,Ekin=None, Epot=None):
    """
    Print and save energy of an nbody particle set during computation
    :param stars: particle set to compute energy on
    :param converter: to convert in nbody units
    :param time: timestep of the computatuion
    :param Ekin: np.array containing kinetic energy of previous steps
    :param Epot: np.array containing potential energy of previous steps
    :return updated kinetic and potential energy arrays
    """

    K = converter.to_nbody(stars.kinetic_energy())
    W = converter.to_nbody(stars.potential_energy(G=constants.G))
    Q = K/-W

    if time == 0 | units.Myr:
        Ekin = [K.number] # equivalent to [K.value_in(nbody_system.energy)]
        Epot = [W.number]
        dE = 0
    else:
        E0 = Ekin[-1] + Epot[-1]
        Ekin.append(K.number)
        Epot.append(W.number)
        dE = ( (K.number+W.number) - E0 ) / E0

    print("time = {:.3f} [Myr], M = {:.3f} [MSun]"
          .format(time.value_in(units.Myr),
                  stars.mass.sum().value_in(units.MSun)))
    print("dE = {:.4f}, Ekin/|Epot| = {:.4f}".format(dE, Q))
    return Ekin, Epot


def log_gas(sph, E0=None):
    """
    Print energy log for hydro simulation
    :param sph: hydro code running
    :param E0: Total energy at prvious timestep
    :return kinetic, potential and thermal energies
    """

    Ekin = sph.unit_converter.to_nbody(sph.kinetic_energy)
    Epot = sph.unit_converter.to_nbody(sph.potential_energy)
    Eth = sph.unit_converter.to_nbody(sph.thermal_energy)
    Egas = Ekin + Epot + Eth
    Qgas = (Ekin+Eth)/-Epot

    dE = (Egas - E0) / np.abs(E0) if E0 else 0
    print("time = {:.3f} [Myr], M = {:.3f} [MSun]"
          .format(sph.get_time().value_in(units.Myr),
                  sph.gas_particles.mass.sum().value_in(units.MSun)))
    print("dE = {:.3f}, Qgas = {:.3f}, Ekin/|Epot| = {}".format(dE, Qgas,
                                                                Ekin/-Epot))
    return Ekin, Epot, Eth



def plot_Edrift(time, Ek, Ep):

    fig, ax = plt.subplots(3, 1, sharex=True, gridspec_kw={'hspace': 0},
                           figsize=(5, 9), tight_layout=True)
    time = np.asarray(time)
    Ek = np.asarray(Ek)
    Ep = np.asarray(Ep)

    ax[0].set_ylabel('potential energy')
    ax[0].plot(time, Ep, c="limegreen")#, marker=m, s=s)
    ax[1].set_ylabel('kinetic energy')
    ax[1].plot(time, Ek, c="mediumvioletred")#, marker=m, s=s)
    # legend_integral(ax[1, 0], 'K', ps.Ekin.value_in(Eunit))
    ax[2].set_ylabel('dE/E')
    ax[2].set_xlabel('time [Myr]')
    ax[2].scatter(time[1:], np.diff(Ek+Ep) / np.abs(Ek+Ep)[:-1],
                  c="orangered")#, marker=m, s=s)
    return ax



################            Multiple system              #######################

def add_companion(parent_stars, mass, semimajor_axis, eccentricity, **kwargs):
    """
    Add a companion to a single or multiple stars
    usefull to make hierarchical multiple system
    :param parent_stars: Particle set to add the companion to
    :param mass: mass of the companion to add
    :param semimajor_axis: semimajor_axis of the new orbiting component
    :param eccentricity: eccentricity of the new orbiting component
    :param kwargs: other parameters of the orbits among (default parameters):
                   true_anomaly = 0 | units.deg,
                   inclination = 0 | units.deg,
                   longitude_of_the_ascending_node = 0 | units.deg,
                   argument_of_periapsis = 0 | units.deg
    :return: a new particle set with the new components
    """

    CoM = Particle()
    CoM.position = parent_stars.center_of_mass()
    CoM.velocity = parent_stars.center_of_mass_velocity()
    CoM.mass = parent_stars.mass.sum()

    nb = new_binary_from_orbital_elements(CoM.mass, mass,
                                          semimajor_axis,
                                          eccentricity,
                                          G=constants.G,
                                          **kwargs)

    new_binary = parent_stars.copy()
    for s in new_binary:
        s.position += nb[0].position
        s.velocity += nb[0].velocity
    new_binary.add_particles(nb[1].as_set())
    return new_binary



#####################             Plots               ##########################

def xy_2stars(stars, ax=None):
    """
    plot position in xy-plane of 2 bodies
    :param stars: particle set to plot
    :param ax: axe of a figure to draw trajectories
    :return: matplotlib's axes
    """

    if ax is None:
        fig, ax = plt.subplots(figsize=(6,6))

    ax.set_xlabel("x [AU]")
    ax.set_ylabel("y [AU]")

    ax.scatter(stars.x.value_in(units.AU),
               stars.y.value_in(units.AU),
               c=('r', 'm'),
               s=50*stars.mass.value_in(units.MSun))

    return ax


def xy_3stars(stars, ax=None):
    """
    plot position in xy-plane of 3 bodies
    :param stars: particle set to plot
    :param ax: axe of a figure to draw trajectories
    :return: matplotlib's axes
    """

    if ax is None:
        fig, ax = plt.subplots(figsize=(6,6))

    ax.set_xlabel("x [AU]")
    ax.set_ylabel("y [AU]")

    ax.scatter(stars.x.value_in(units.AU),
               stars.y.value_in(units.AU),
               c=('r', 'm', 'b'),
               s=50*stars.mass.value_in(units.MSun))

    return ax


def sph_to_grid(sph_code, view=[-1., 1., -1., 1., 0, 0] | units.pc,
                grid_size=(128, 128, 1)):
    """
    Interpolate SPH representation on a defined cartesian grid
    :param sph_code: sph code to make the interpolation of the sph representation
    :param view: the (physical) region to sample [xmin, xmax, ymin, ymax, zmin, zmax] | units
    :param grid_size:  discretisation of the cube in pixels (x, y, z)
    :return: density map (array of shape grid_size),
             velocity maps (3 arrays of shape grid_size)
             length of cells in each direction (array in amuse units)
    """

    shape = (grid_size[0], grid_size[1], grid_size[2])
    size1D = grid_size[0] * grid_size[1] * grid_size[2]
    axis_lengths = [0.0, 0.0, 0.0] | units.pc
    axis_lengths[0] = view[1] - view[0]
    axis_lengths[1] = view[3] - view[2]
    axis_lengths[2] = view[5] - view[4]
    grid = new_regular_grid(shape, axis_lengths)
    grid.x += view[0]
    grid.y += view[2]
    grid.z += view[4]
    speed = grid.z.reshape(size1D) * (0 | 1 / units.s) #convert into speed units

    rho, rhovx, rhovy, rhovz, rhoe = \
        sph_code.get_hydro_state_at_point(grid.x.reshape(size1D),
                                          grid.y.reshape(size1D),
                                          grid.z.reshape(size1D),
                                          speed, speed, speed)
    vx = rhovx / rho
    vy = rhovy / rho
    vz = rhovz / rho
    rho = rho.reshape(shape)
    vx = vx.reshape(shape)
    vy = vy.reshape(shape)
    vz = vz.reshape(shape)
    # physical size of each cells
    cells_length = [axis_lengths[i] / grid_size[i] for i in range(3)]

    return rho, (vx, vy, vz), cells_length


def plot_hydro(rho, extent, time=0|units.Myr, vmin=None, vmax=None, stars=None, ax=None):
    """
    Plot 2D density map in logarithmic scale
    :param rho: density map to plot
    :param extent: physical limits of the plot [xmin, xmax, ymin, ymax] | units
    :param time: time of the snapshot
    :param vmin: sets the minimum density value
    :param vmax: sets the maximum density value
    :param stars: particle set to superimpose their position on the plot
    :return: matplotlib's fig and ax
    """

    if ax is None:
        fig, ax = plt.subplots(figsize=(6,6))
    rho[rho == 0] = np.nan
    im = ax.imshow(np.log10(rho.T),
                   extent=extent.value_in(units.pc),
                   origin="lower",
                   vmin=vmin, vmax=vmax
                   )

    if ax is None:
        cbar = plt.colorbar(im)
        cbar.set_label('density [$cm^-3$]', rotation=270)

    if stars and not stars.is_empty():
        ax.scatter(
                stars.x.value_in(units.pc),
                stars.y.value_in(units.pc),
                s=10*stars.mass.value_in(units.MSun),
                edgecolors=None,
                color="w",
        )
    ax.set_title("Molecular cloud density at time={:4.3f}Myr".format(time.value_in(units.Myr)))
    ax.set_xlabel("x [pc]")
    ax.set_xlim(extent.value_in(units.pc)[:2])
    ax.set_ylabel("y [pc]")
    ax.set_ylim(extent.value_in(units.pc)[-2:])
    return ax
