# coding: utf8
"""

"""
import numpy as np
import matplotlib.pyplot as plt

from amuse.datamodel import Particles, Particle
from amuse.io import write_set_to_file, read_set_from_file

from amuse.community.ph4.interface import ph4
from amuse.community.bhtree.interface import Bhtree

from amuse.units import units, nbody_system, constants
from amuse.support.console import set_printing_strategy
set_printing_strategy("custom", preferred_units=[units.MSun, units.parsec, units.Myr],
                      precision=4, prefix="", separator="[", suffix="]")

from help_functions import *


if __name__ == "__main__":

    stars = # Load your initial conditions,

    # We set a converter since the majority of codes have their own system of units,
    # so we just choose 2 out of the 3 basic quantities (between mass, length, time)
    # to constrain the conversion from a system to another
    converter = nbody_system.nbody_to_si(stars.mass.sum(), 100 | units.AU)

    # Initialise the code and communication
    gravity = ph4(converter)
    gravity.particles.add_particles(stars)
    to_stars = gravity.particles.new_channel_to(stars)

    # save initial configuration
    t = 0 | units.Myr
    write_set_to_file(stars.savepoint(t), "test1_binary.hdf5",
                      format="amuse", version=2)

    # plot position
    axy = xy_2stars(stars)
    # print energy
    Ek, Ep = log_nbody(stars, converter)

    # evolve in a loop
    time = np.arange(0, 10, 0.1) | units.kyr
    for t in time:
        gravity.evolve_model(t)
        to_stars.copy()
        write_set_to_file(stars.savepoint(t), 'test1_binary.hdf5',
                          append_to_file=True,
                          format="amuse", version=2)
        xy_2stars(stars, axy)
        plt.draw()
        plt.pause(0.001)

    # stop the code
    gravity.stop()



    ############################################################################
    ###########         Post-processing of the computation           ###########
    ############################################################################
    # stars = read_set_from_file("test1_binary.hdf5", format="amuse", version=2)
    # Ek, Ep, dE = [], [], []
    # for s in stars.history:
    #     t = s.get_timestamp()
    #     Ek, Ep, dE = log_nbody(s, converter, t, Ek, Ep, dE)
    # plot_Edrift(time.value_in(units.Myr), Ek, Ep)


    ############################################################################
    #####        Other initial conditions: equilateral triangle           ######
    ############################################################################
    # stars = Particles(3)
    # stars.mass = [1, 1, 1] | units.MSun
    # stars.x = [-3**0.5/4, 0, 3**0.5/4] | units.pc
    # stars.y = [-1/4, 1/2, -1/4] | units.pc
    # stars.z = [0, 0, 0] | units.pc
    # stars.vx = np.random.uniform(-0.05, 0.05, 3) | units.kms
    # stars.vy = np.random.uniform(-0.05, 0.05, 3) | units.kms
    # stars.vz = [0, 0, 0] | units.kms


    ############################################################################
    #####        Other initial conditions: hierachical triple             ######
    ############################################################################

    # primary = Particles(mass=m0,
    #                     x = 0|units.pc,
    #                     y = 0|units.pc,
    #                     z = 0|units.pc,
    #                     vx = 0|units.kms,
    #                     vy = 0|units.kms,
    #                     vz = 0|units.kms)
    #
    # binary = add_companion(primary, m1, a, e)
    # triple = add_companion(binary, 0.3|units.MSun, 500, 0.9,
    #                        true_anomaly=45|units.deg)
