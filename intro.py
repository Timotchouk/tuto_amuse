# coding: utf8
"""
Create a binary system (simple introduction script to AMUSE)
- Introduction to units
- Defining a Particle set
"""
import numpy as np
import matplotlib.pyplot as plt

from amuse.datamodel import Particles

from amuse.ic.gasplummer import new_plummer_gas_model
from amuse.units import units, nbody_system, constants
from amuse.support.console import set_printing_strategy
set_printing_strategy("custom", preferred_units=[units.MSun, units.parsec, units.Myr],
                      precision=4, prefix="", separator="[", suffix="]")


if __name__ == "__main__":

    # Define the properties of 2 components of a binary
    m0 = 1 | units.MSun
    m1 = 0.6 | units.MSun
    a = 100. | units.AU
    e = 0.9

    # Compute basic quantities of the binary
    G = constants.G
    Mt = m0 + m1
    T = np.pi*2*( a**3 / (G * Mt) )**0.5
    mu = G * Mt

    # Compute the position of each components at apocenter
    r_apo = a * (1+e)
    r0 = m1/Mt * r_apo * [1.0, 0.0, 0.0]
    r1 = m0/Mt * r_apo * [-1.0, 0.0, 0.0]

    # Compute the velocity of each components at apocenter
    v_apo = np.sqrt(mu / a * ( (1-e) / (1+e) ))
    v0 = m1/Mt * v_apo * [0.0, 1.0, 0.0]
    v1 = m0/Mt * v_apo * [0.0, -1.0, 0.0]

    # create an AMUSE particle set
    binary = Particles(2)
    binary.mass = [m0, m1]
    binary[0].position = r0
    binary[0].velocity = v0
    binary[1].position = r1
    binary[1].velocity = v1
    print(binary)

    # Simple plot
    fig, ax = plt.subplots()
    ax.scatter(binary.x.value_in(units.AU),
               binary.y.value_in(units.AU),
               s=50*binary.mass.value_in(units.MSun))
    ax.scatter(binary.center_of_mass()[0].value_in(units.AU),
               binary.center_of_mass()[1].value_in(units.AU),
               marker='x', c='k')
    ax.quiver(binary.x.value_in(units.AU),
              binary.y.value_in(units.AU),
              binary.vx.value_in(units.kms),
              binary.vy.value_in(units.kms),
              )
    plt.show()



