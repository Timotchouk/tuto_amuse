The Gravitationnaly-Driven Fragmentation (GDF) model is a method to produce self-consistent fragmented distributions of stars using only N-body dynamics. These models can be used to reproduce the self-similar structure of young clusters at the end of the star formation process. They are particularly suited to study the dynamical processes within these regions since the velocity field is consistently built in the same time than the spatial fragmented distribution, leadind to coherent 6-D phase space configuraions.

More details about the method can be found here:
https://ui.adsabs.harvard.edu/abs/2016MNRAS.459.1213D/abstract
https://ui.adsabs.harvard.edu/abs/2020A%26A...644A.141R/abstract


 
