#!/usr/bin/python3
# coding: utf8
"""
Script to test the precision of different n-body codes on a Plummer sphere during 1 nbody time
"""
import numpy as np
import matplotlib.pyplot as plt
import time

from amuse.ic.plummer import new_plummer_model
from amuse.units import nbody_system
from amuse.support.console import set_printing_strategy

set_printing_strategy("custom", preferred_units=[u.MSun, u.AU, u.yr],
                      precision=9, prefix="", separator="[", suffix="]")

from amuse.community.ph4.interface import ph4
from amuse.community.huayno.interface import Huayno
from amuse.community.hermite.interface import Hermite
from amuse.community.phigrape.interface import PhiGRAPE
from amuse.community.sakura.interface import Sakura

if __name__ == '__main__':

    # Create a Plummer sphere in equilibrium in a Particle set (in nbody units by default)
    Nstars = 1000
    plummer = new_plummer_model(Nstars)
    plummer.scale_to_standard()
    plummer.move_to_center()
    plummer.scale_to_standard()

    # List the code to test (without parenthesis whereas they will be run as a subroutine)
    codes = [ph4, Hermite, PhiGRAPE, Huayno, Sakura]
    Edrift = {'ph4':[], 'ph4_GPU':[], 'Hermite':[], 'PhiGRAPE':[], 'Huayno':[], 'Sakura':[]}
    computation_time = {'ph4':[], 'ph4_GPU':[], 'Hermite':[], 'PhiGRAPE':[], 'Huayno':[], 'Sakura':[]}

    dt = 1. | nbody_system.time
    t_end = 10. | nbody_system.time
    # Test different timestep parameter
    precision = [0.32, 0.16, 0.08, 0.04, 0.02, 0.01, 0.005, 0.0025, 0.001]

    for p in precision:
        for code in codes:
            if code == 'ph4_GPU':
                gravity = ph4(mode='gpu', number_of_workers=1)
                codename = 'ph4_GPU'
            else:
                gravity = code(number_of_workers=4)# new_smalln()
                codename = gravity.model_name

            # Here we specify the precision of the integrator.
            # It is linked to the choice of the timestep for the integration.
            # This is code dependant, hence the different parameter name.
            if codename is 'Hermite':
                gravity.parameters.dt_param = precision
            elif codename is 'Sakura':
                gravity.parameters.timestep = precision/10 | nbody_system.time
            else:
                gravity.parameters.timestep_parameter = precision

            # As we will evolve the plummer system, we make a copy of the original particle set
            # to be able to repeat the loop with a different code
            stars = plummer.copy()

            # Adding the system to the gravity solver
            gravity.particles.add_particles(stars)

            # We create a channel from the particle set within the gravity solver (gravity.particles)
            # to the stars particle set in order to be able to update the phase space configuration of the system.
            channel = gravity.particles.new_channel_to(stars)

            # We compute the initial total energy of the system to check the integration precision
            E = [stars.kinetic_energy() + stars.potential_energy(G=nbody_system.G)]

            # initial time of the system
            t = 0 | nbody_system.time
            t0 = time.time()
            while t < t_end:
                t += dt
                # we integrate the system until time "t". Pay attention, it is an absolut time not a timestep !
                gravity.evolve_model(t)

                # We update the phase space configuration of the system
                channel.copy()

                # We compute the new total energy of the system
                E.append(stars.kinetic_energy() + stars.potential_energy(G=nbody_system.G))

            # Compute the integration time
            comptime = (time.time()-t0) / 10
            computation_time[codename].append(comptime)
            print(codename, comptime)

            # Compute the mean energy difference between 2 timesteps
            dE = np.mean(np.abs(np.diff(E))/E[:-1])
            Edrift[codename].append(dE)

            # Do not forget to stop the gravity solver in order to stop the worker that
            # work outside of the python script !
            gravity.stop()
