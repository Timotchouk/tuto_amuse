#!/usr/bin/python3.8
# coding: utf8
"""
Build a GDF model model with the multiple module
"""
import pickle
import numpy as np
import matplotlib.pyplot as plt
import time as check

from amuse.community.ph4.interface import ph4
from amuse.datamodel import Particles, ParticlesWithUnitsConverted
from amuse.io import write_set_to_file
import amuse.units.nbody_system as nbo
import amuse.units.units as u
from amuse.support.console import set_printing_strategy
set_printing_strategy("custom", preferred_units=[u.MSun, u.parsec, u.Myr],
                      precision=9, prefix="", separator="[", suffix="]")

from GDF_functions import *

def plot_XY(stars, t_si, R_lim):
    """
    Plot on a matplotlib figure the xy-coordinate of a particle set
    :param stars: particle set to plot
    :param t_si: time label
    :param R_lim: spatial limits of the figure
    :return: Figure and axe
    """
    fig, ax = plt.subplots()
    ax.scatter(stars.x.value_in(u.pc),
               stars.y.value_in(u.pc),
               s=stars.mass.value_in(u.MSun)*10,
               edgecolors='k', facecolor=(0.8, 0.8, 0.8, 0.5))
    ax.set_xlim([-R_lim, R_lim])
    ax.set_ylim([-R_lim, R_lim])
    ax.set_xlabel('X [pc]')
    ax.set_ylabel('Y [pc]')
    ax.set_title("t = {:1.2f}".format(t_si.value_in(u.Myr)))
    return fig, ax


if __name__ == "__main__":

    # Initial parameters
    saving_dir = "/home/Users/data/test_GDF_model/"
    simuname = 'GDF_direct'
    fig_dir = saving_dir + "fig/"
    Nstar = 1000
    unit = nbo.length
    Mmax = 10 | u.MSun
    Mmin = 0.01 | u.MSun
    L = 0.1 | u.parsec
    H0 = 1 | nbo.time**-1
    grav_code = ph4
    mode = 'cpu'
    nbr_w = 4
    timestep_param = 0.1
    eps = 0. | nbo.length
    snap_dt = .01 | nbo.time

    # Generate initial conditions
    uni_sphere = GDF_ic(Nstar, H0)
    t_apex, R_apex = apex_GDF(H0.number)
    # Give masses to the stars
    MF, _, _ = maschberger_imf(Nstar, mass_min=Mmin, mass_max=Mmax)
    Mtot = MF.sum()
    # Normalise star masses to be in Hénon unit
    uni_sphere.mass = (MF / Mtot) | nbo.mass

    # Create a particle set in SI units
    conv = nbo.nbody_to_si(Mtot, L)
    uni_sphere_si = ParticlesWithUnitsConverted(uni_sphere,
                                                conv.as_converter_from_si_to_nbody())

    # Print some usefull quantities
    t = 0 | nbo.time
    time = [0]
    print("Time: ", conv.to_si(t))
    print('Nstars:', len(uni_sphere), " - Mtot:", Mtot)
    E0, Q, dE, ek, ep = log_simple(uni_sphere)
    Etot = [E0.number]
    dEtot = [dE]
    Qvir = [Q]
    Ek = [ek.number]
    Ep = [ep.number]

    # Create files to save the phase space configuration evolution during the simulation
    # Save initial conditions
    filename = saving_dir+simuname
    try:
        write_set_to_file(uni_sphere_si.savepointsavepoint(t),
                          filename,
                          'amuse', version=2.0)
        print("Amuse file created in '{}'".format(saving_dir+simuname))
    except FileExistsError as e:
        raise FileExistsError("simulation results already exist, "
                              "don't overwrite results at t=0") from e

    # Plot scatter plot of the XY-plane
    plt.ioff()
    R_lim = (conv.to_si(R_apex) * 1.1).value_in(u.parsec)
    fig, ax = plot_XY(uni_sphere_si, conv.to_si(t), R_lim)
    fig.savefig(fig_dir + "{}_{:06.3f}".format(simuname, 0).replace('.', ''))
    plt.close()

    # Initialise gravity solver
    gravity = grav_code(mode=mode, number_of_workers=4)
    gravity.parameters.epsilon_squared =  eps ** 2
    gravity.parameters.timestep_parameter = timestep_param

    # Add the initial conditions within the code
    gravity.particles.add_particles(uni_sphere)
    # Set the channel to update data between the 2
    to_stars = gravity.particles.new_channel_to(uni_sphere)

    # Evolve the cluster, make several snapshots of the evolution
    t0 = check.time()
    while t < t_apex:

        t += snap_dt
        gravity.evolve_model(t)
        # Update the phase space configuration
        to_stars.copy()

        # Save the updated particle set and some usefull quantities
        try:
            write_set_to_file(uni_sphere_si.savepointsavepoint(t),
                              filename,
                              'amuse', version=2.0,
                              append_to_file=True)
            print("Amuse file '{}' updated".format(filename))
        except OSError:
            print("AMUSE FILE '{}' NOT UPDATED".format(filename))

        print("Time: ", conv.to_si(t))
        print('Nstars: ', len(uni_sphere), "Mtot: ", Mtot)
        time.append(t.number)
        E0, Q, dE, ek, ep = log_simple(uni_sphere, E0=E0)
        Etot.append(E0.number)
        Qvir.append(Q)
        dEtot.append(dE)
        Ek.append(ek.number)
        Ep.append(ep.number)

        # Save a scatter plot
        fig, ax = plot_XY(uni_sphere_si, conv.to_si(t), R_lim)
        fig.savefig(fig_dir + "{}_{:06.3f}".format(simuname, t.number.replace('.', ''))
        plt.close()

        print('<>' * 30, '\n')

    # Compute the CPU time of the computation
    comptime = check.time() - t0
    print('COMPUTATION FINISHED: {}h {}min {:2.0f}sec'
          .format(int(np.floor(comptime / 3600.)),
                  int(np.floor(comptime % 3600. / 60)),
                  comptime % 3600 % 60))
    # Shut down the subprocess of the N-body code
    gravity.stop()

    # Save energy log (in Hénon unit) in a dictionnary and then in a pickle file
    energy = {"time": np.asarray(time),
              "Etot": np.asarray(Etot),
              "dE": np.asarray(dEtot),
              "Ek": np.asarray(Ek),
              "Ep": np.asarray(Ep)}
    with open(saving_dir+"energy_log", 'wb') as f:
        pickle.dump(energy, f)

    # Plot the energy drift to control the accuracy of the computation
    fig, ax = E_drift(energy)
    fig.savefig(saving_dir+"E_drift")


