#!/usr/bin/python3.8
# coding: utf8
"""
Build a GDF model model with the multiple module.
It processes differently single stars an multiple system which are compute in
the center of mass (CM) hypothesis. The Multiple module uses 3 codes:
 - the top-level solver integrates all CM (choose a general direct N-body code)
 - the small-N computes encounters between stars and create multiple system
 - the kepler code computes the orbital parameters of the multiple system
   (higher order systems are always hierarchical and can be described
   as multiple binary system).
Single stars and CM of multiple system with an extra attribute in the particle set
which is id. for multiple system id>1000000.
More infos on the class Multiples which host the multiple module under amuse/couple/multiples
"""

import numpy as np
import matplotlib.pyplot as plt
import time as check
import pickle

from amuse.couple import multiples as modemulti
from amuse.community.kepler.interface import Kepler
from amuse.community.ph4.interface import ph4
from amuse.community.smalln.interface import SmallN

from amuse.io import read_set_from_file, write_set_to_file
from amuse.datamodel import Particles, ParticlesWithUnitsConverted
from amuse.units import nbody_system as nbo, units as u, constants

from amuse.support.console import set_printing_strategy
set_printing_strategy("custom", preferred_units=[u.MSun, u.parsec, u.Myr],
                      precision=9, prefix="", separator="[", suffix="]")
                      
from GDF_functions import *


# We use a particular solver to compute encounter between particles call SmallN
# It needs these function to work properly
SMALLN = None
def init_smalln(converter):
    global SMALLN
    SMALLN = SmallN(convert_nbody=converter)

def new_smalln():
    SMALLN.reset()
    return SMALLN

def stop_smalln():
    global SMALLN
    SMALLN.stop()


def newCMpos(multi_sys, CM_now):
    """
    Update the position and velocity of the center of mass of a multiple system
    to its new position.
    :param multi_sys: Multiple system in a Particle set at the time of its creation,
    first particle is the center_of mas the 2nd and 3rd, the inner components
    :param CM_now: Position of the particle set at new time
    :return: Updated CM
    """
    CM_at_encou = multi_sys[0]

    dx = CM_now.x -  CM_at_encou.x
    dy = CM_now.y -  CM_at_encou.y
    dz = CM_now.z - CM_at_encou.z
    dvx = CM_now.vx - CM_at_encou.vx
    dvy = CM_now.vy - CM_at_encou.vy
    dvz = CM_now.vz - CM_at_encou.vz

    multi_sys.x += dx
    multi_sys.y += dy
    multi_sys.z += dz
    multi_sys.vx += dvx
    multi_sys.vy += dvy
    multi_sys.vz += dvz

    return CM_at_encou


def plot_XY(singles, bina, multi, t_si, R_lim):
    """
    Plot on a matplotlib figure the xy-coordinate of a particle set
    :param singles: single particles in black
    :param bina: binary particles in red (plot only center of mass)
    :param multi: other multiple system in orange (plot only center of mass)
    :param t_si: time label
    :param R_lim: spatial limits of the figure
    :return: Figure and axe
    """
    fig, ax = plt.subplots()
    ax.scatter(singles.x.value_in(u.pc),
               singles.y.value_in(u.pc),
               s=singles.mass.value_in(u.MSun)*10,
               edgecolors='k', facecolor=(0.8, 0.8, 0.8, 0.5))
    if not bina.is_empty():
        ax.scatter(bina.x.value_in(u.pc),
                   bina.y.value_in(u.pc),
                   s=bina.mass.value_in(u.MSun)*10,
                   edgecolors='r', facecolor=(0.8, 0.8, 0.8, 0.5))
    if not multi.is_empty():
        ax.scatter(multi.x.value_in(u.pc),
                   multi.y.value_in(u.pc),
                   s=multi.mass.value_in(u.MSun)*10,
                   edgecolors='orange', facecolor=(0.8, 0.8, 0.8, 0.5))
    ax.set_xlim([-R_lim, R_lim])
    ax.set_ylim([-R_lim, R_lim])
    ax.set_xlabel('X [pc]')
    ax.set_ylabel('Y [pc]')
    ax.set_title("t = {:1.2f}".format(t_si.value_in(u.Myr)))
    return fig, ax


def log_multi(code, conv, E0=None):
    """
    Simple diagnostics on multiples
    """
    ke = conv.to_nbody(code.kinetic_energy)
    pe = conv.to_nbody(code.potential_energy)
    Q = - ke / pe
    Nmul, Nbin, Emul = code.get_total_multiple_energy2()
    print('\n'*2,'<>'*40,'\n'*2)
    print('Time =', code.get_time())
    print('    top-level kinetic energy =', ke)
    print('    top-level potential energy =', pe)
    print('    total top-level energy =', ke + pe)
    print('   ', Nmul, 'multiples')
    print('   ', Nbin, 'binaries,')
    print('      total multiple energy =', Emul)
    E = ke + pe + conv.to_nbody(Emul)
    print('    uncorrected total energy =', E)

    # Apply known corrections.
    Etid = code.multiples_external_tidal_correction \
           + code.multiples_internal_tidal_correction  # tidal error
    Eerr = code.multiples_integration_energy_error  # integration error

    Ecorr = E - conv.to_nbody(Etid + Eerr)
    print('    corrected total energy =', Ecorr)
    dE = (Ecorr - E0) / E0 if E0 is not None else 0
    print('    relative energy error=', dE)

    return Ecorr, Q, dE


def count_multi(code, stars, Nstar):
    """
    Count number of binaries, and higher order multiples
    plus the id of the multiples (check missing stars)
    """
    l = []
    for mul in stars[stars.id > 1000000]:
        l.append(len(code.root_to_tree[mul].get_leafs_subset()))
    l = np.asarray(l)
    multN, count = np.unique(l, return_counts=True)
    print("binaries:", count[multN==2])
    print("triples:", count[multN == 3])
    print("quadruples:", count[multN == 4])
    for b in multN[multN > 4]:
        print("multiples of {} stars: {}".format(b, count[multN == b]))
    N = len(stars[stars.id < 1000000]) + sum(multN * count)
    print("Nstar :", N)
    if N != Nstar:
        raise RuntimeError("Multiple module did not recover all stars, "
                           "missing stars: ", Nstar-N)
    else:
        return N


if __name__ == "__main__":

    # Initial parameters
    saving_dir = "/home/Users/data/test_GDF_model/"
    simuname = 'GDF_multi'
    fig_dir = saving_dir + "fig/"
    Nstar = 10000
    unit = nbo.length
    Mmax = 10 | u.MSun
    Mmin = 0.01 | u.MSun
    L = 0.1 | u.parsec
    H0 = 1 | nbo.time**-1
    snap_dt = .01 | nbo.time
    grav_code = ph4
    mode = 'cpu'
    nbr_w = 2 # We will use 2 other code within the multiple module, if only 4
    # cores are available we need to reduce the number of cores used by the
    # N-body solver accordingly
    timestep_param = 0.1
    eps = 0. | nbo.length

    # Generate initial conditions
    uni_sphere = GDF_ic(Nstar, H0)
    t_apex, R_apex = apex_GDF(H0.number)
    # Give masses to the stars
    MF, _, _ = maschberger_imf(Nstar, mass_min=Mmin, mass_max=Mmax)
    Mtot = MF.sum()
    # Normalise star masses to be in Hénon unit
    uni_sphere.mass = (MF / Mtot) | nbo.mass
    
    # Multiple systems are identified by an id>1000000, so it is important to
    # set this attribute
    uni_sphere.id = np.arange(1, Nstar+1)

    # the module detects collision based on a collisionnal radius.
    # For simplicity, we set it proportionnal to the mass of the star
    uni_sphere.radius = uni_sphere.mass.number / 2 | nbo.length

    # Create a particle set in SI units
    conv = nbo.nbody_to_si(Mtot, L)
    CoMs = ParticlesWithUnitsConverted(uni_sphere,
                                          conv.as_converter_from_si_to_nbody()).copy()

    # Print some usefull quantities
    t = 0 | nbo.time
    time = [0]
    print("Time: ", conv.to_si(t))
    print('Nstars:', len(uni_sphere), " - Mtot:", Mtot)
    E0, Q, dE, ek, ep = log_simple(uni_sphere)
    Etot = [E0.number]
    dEtot = [dE]
    Qvir = [Q]
    Ek = [ek.number]
    Ep = [ep.number]


    # Create files to save the phase space configuration during the simulation
    # we create 3 different particle sets which will be updated accordingly.
    # we split CoMs in 3 different particle sets
    singles = CoMs[CoMs.id<1000000].copy()

    # For the moment there are no multiple systems
    bina = Particles()
    multi = Particles()

    filename_singl = saving_dir + simuname + '_singles'
    filename_bina = saving_dir + simuname + '_binaries'
    filename_multi = saving_dir + simuname + '_multiples'
    try:
        write_set_to_file(singles.savepoint(t),
                          filename_singl,
                          'amuse', version=2.0)
        print("Amuse file created in '{}'".format(filename_singl))

        write_set_to_file(bina.savepoint(t),
                          filename_bina,
                          'amuse', version=2.0)
        print("Amuse file created in '{}'".format(filename_bina))

        write_set_to_file(multi.savepoint(t),
                          filename_multi,
                          'amuse', version=2.0)
        print("Amuse file created in '{}'".format(filename_multi))

    except FileExistsError as e:
        raise FileExistsError("simulation results already exist, "
                              "don't overwrite results at t=0") from e

    # Plot scatter plot of the XY-plane
    # plt.ioff()
    R_lim = (conv.to_si(R_apex) * 1.1).value_in(u.parsec)
    fig, ax = plot_XY(singles, bina, multi, conv.to_si(t), R_lim)
    fig.savefig(fig_dir + "{}_{:06.3f}".format(simuname, 0).replace('.', ''))
    plt.close()

    # Initialize top-level solver
    gravity = ph4(convert_nbody=conv, mode=mode, number_of_workers=nbr_w)
    gravity.parameters.epsilon_squared = eps ** 2
    gravity.parameters.timestep_parameter = timestep_param

    # Add the initial conditions within the code
    gravity.particles.add_particles(CoMs)

    # Enable the stopping condition when the distance between 2 particles is
    # less than the sum of their radius
    stop_cond = gravity.stopping_conditions.collision_detection
    stop_cond.enable()

    # Initialize the small-N integrator and the Kepler code
    init_smalln(conv)
    kep = Kepler(unit_converter=conv)
    kep.initialize_code()

    # Initialise the Multiple module
    multicode = modemulti.Multiples(gravity, new_smalln, kep, gravity_constant=constants.G)

    perturb_limit=0.02
    # the multiple module checks before the potential creation of a binary (1 & 2),
    # if one or more other particles (3) can perturb the encounter, based on this criterion:
    # if perturb_limit * (m1+m2/a**3) > m3/d**3 (where m are the mass of the particles,
    # a the semi-major axis of the  binary and d the distance from the perturber to the binary)
    # this test is done for all particles around the binary, for those which passes
    # they are considered as potential perturbers (see line 934 to 965 of couples/multiples.py)
    multicode.neighbor_perturbation_limit = perturb_limit

    neighbor_veto=True
    # Then if neighbour_veto is True, any presence of a perturber avoid
    # the multiple module to create a stable multiple system, if False the module
    # include the perturber to the SmallN calculation. Nevertheless this leads
    # (at the time of the creation of this script ~2021) to unrealistically
    # numerous multiple systems (see line 975 to 995 of couples/multiples.py)
    multicode.neighbor_veto = neighbor_veto

    # set the level of verbosity of the multiple module
    multicode.global_debug = 2

    # Set the channel to update data between module and the CoMs particle set
    to_stars = multicode.particles.new_channel_to(CoMs)

    # Evolve the cluster, make several snapshots of the evolution
    t0 = check.time()
    while conv.to_nbody(multicode.get_time()) < t_apex:
        # launch evolution
        t += snap_dt
        multicode.evolve_model(conv.to_si(t))

        # Before update data we have to update the list of top level particles.
        # Indeed, if two particles are in a binary after the evolution, they are
        # now replaced by the center of mass of the new binary. This method update
        # the list of particles and accounts for the creation of new particles
        multicode.particles.synchronize_to(CoMs)

        # Now we can update the phase-space of the top level particles,
        # the previous method does not update the phase space configuration
        # just the list of particles
        to_stars.copy()

        # The module manage id of CM of multiple systems as "index_in_code"
        # We want to retrieve them in our particle set as "id"
        # id of multiple system are >1000000
        to_stars.copy_attribute("index_in_code", "id")

        # Print hierarchical structure of multiple module
        multicode.print_trees_summary()

        # Create specific particle sets for multiple systems
        bina = Particles()
        multi = Particles()

        # recover the inner components of all multiple system created and save
        # them as attributes of the macro-particule representing their center of mass
        # the Multiple module save all encounter in a dictionnary named "root_to_tree"
        for binatree in multicode.root_to_tree.values():
            # recover center_of mass and inner component of a multiple system in
            # one particle set
            structure = binatree.get_tree_subset().copy()

            # update the new position of the CM (because it has moved since
            # the encounter), the root_to_tree save its position at
            # the time of the encounter
            new_CM = newCMpos(structure, structure[0].as_particle_in_set(CoMs))

            # Add CM of multiple system according to the number of their inner components
            if len(structure) == 3:
                bina.add_particle(new_CM)
            elif len(structure) > 3:
                multi.add_particle(new_CM)

        # Check if multiple module recovers all the particles
        n = count_multi(multicode, CoMs, Nstar)

        # Save particle set with all memory of the structure
        singles = CoMs[CoMs.id<1000000].copy()
        try:
            write_set_to_file(singles.savepoint(t), filename_singl,
                              'amuse', version=2.0,
                              append_to_file=True)
            print("Amuse file created in '{}'".format(filename_singl))

            write_set_to_file(bina.savepoint(t), filename_bina,
                              'amuse', version=2.0,
                              append_to_file=True)
            print("Amuse file created in '{}'".format(filename_bina))

            write_set_to_file(multi.savepoint(t), filename_multi,
                              'amuse', version=2.0,
                              append_to_file=True)
            print("Amuse file updated in '{}'".format(filename_multi))

        except OSError:
            print("AMUSE FILE '{}' NOT UPDATED")

        # Get energy logs
        E0, Q, dE = log_multi(multicode, conv, E0)
        time.append(conv.to_nbody(multicode.get_time()).number)
        Etot.append(E0.number)
        Qvir.append(Q)
        dEtot.append(dE)
        print('Total mass', CoMs.mass.sum())

        # Save a scatter plot
        fig, ax = plot_XY(singles, bina, multi, conv.to_si(t), R_lim)
        fig.savefig(fig_dir + "{}_{:06.3f}".format(simuname, t.number.replace('.', ''))
        plt.close()
        print('<>' * 30, '\n')

    # Compute the CPU time of the computation
    comptime = check.time() - t0
    print('COMPUTATION FINISHED: {}h {}min {:2.0f}sec'
          .format(int(np.floor(comptime / 3600.)),
                  int(np.floor(comptime % 3600. / 60)),
                  comptime % 3600 % 60))

    # Shut down the subprocess of all codes
    gravity.stop()
    kep.stop()
    stop_smalln()

    # Save energy log (in Hénon unit) in a dictionnary and then in a pickle file
    energy = {"time": np.asarray(time),
              "Etot": np.asarray(Etot),
              "dE": np.asarray(dEtot),
              "Ek": np.asarray(Ek),
              "Ep": np.asarray(Ep)}
    with open(saving_dir+"energy_log", 'wb') as f:
        pickle.dump(energy, f)

    # Plot the energy drift to control the accuracy of the computation
    fig, ax = E_drift(energy)
    fig.savefig(saving_dir+"E_drift")


    # Plot MST edges
    # ax = edges_distrib(mst)

    # fig, ax = plt.subplots()
    # ax.set_xlabel('time [nbody units]')
    # ax.set_ylabel('Energy variations [nbody units]')
    # ax.plot(np.transpose(2*[time]), np.transpose([dEtot, Etot-np.mean(Etot)]))
    # ax.legend(['$dE = dfrac{{(E_i - E_{i-1}} {E_{i-1}} }$', '$E_mean = {0}'.format(np.mean(Etot))])
    # # format the ticks label
    # # from matplotlib.ticker import ScalarFormatter
    # # ScalarFormatter(useMathText=True)
    # # form = ScalarFormatter(useMathText=True)
    # # form.set_powerlimits((-1, 1))
    # # ax.yaxis.set_major_formatter(form)
    # fig.savefig(ps['savin']+"fig/{}_energylog".format(ps['simn']), dpi=100)
    # plt.close()
