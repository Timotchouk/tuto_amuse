# coding: utf8
def GDF_ic(Nstar, H0, R0=1|nbo.length):
    """
    Create a uniformly populated sphere of stars with radial velocities

    These initial conditions provide an expansion of a star cluster leading to
    form auto-coherent fragmented configuration of stars or the so-called
    gravity-driven fragmentation (GDF) models
    :param Nstar: Number of stars in the cluster
    :param H0: expansion parameter in time^-1
           (looks like the hubble parameter in cosmology)
    :param R0: physical size of the initial sphere
    :param imf: mass functinal arguments for the MF (mass_min, mass_max...)
    :return: initial phase spac configuration e of a Hubble-Lemaitre expansion
             in an AMUSE particle set
    """
    # Give distance to center R
    X = np.random.uniform(0, 1, Nstar)
    R = R0 * X ** (1. / 3)

    # Give random angles to stars
    X1 = np.random.uniform(0, 1, Nstar)
    X2 = np.random.uniform(-1, 1, Nstar)
    phi = 2 * np.pi * X1
    theta = np.arccos(X2)

    # convert into cartesian coordinates
    x = R * np.cos(phi) * np.sin(theta)
    y = R * np.sin(phi) * np.sin(theta)
    z = R * np.cos(theta)

    # Build amuse particle set
    uni_sphere = Particles(Nstar)
    uni_sphere.x = x
    uni_sphere.y = y
    uni_sphere.z = z
    uni_sphere.vx = H0 * x
    uni_sphere.vy = H0 * y
    uni_sphere.vz = H0 * z
    return uni_sphere


def apex_GDF(H0):
    """
    Compute theoretical timescale and radius of GDF models at the end of the
    expansion phase (in Hénon units)
    :param H0: expansion parameter in [1/nbody_system.time]
    :return: apex time and radius in Hénon units
    """
    E = 2 / H0**2
    x_apex = E/(E-1)
    theta = np.arcsin(1 / x_apex**0.5)
    t_apex = (E*(np.pi/2 - theta) + (E-1)**0.5) / H0*(E-1)**(-3/2)
    return t_apex | nbo.time, x_apex | nbo.length


def maschberger_imf(N, mass_min=0.01|u.MSun, mass_max=125|u.MSun, mu=0.2|u.MSun):
    """
    Compute the IMF according to the L3 Maschberger 2013 description
    (http://adsabs.harvard.edu/abs/2013MNRAS.429.1725M)
    :arg N: number of stars
    Truncation values for the stellar masses :
    :arg mass_max: upper bound
    :arg mass_min: lower bound
    :arg mu: mean mass of all stars
    :return: MF [u.MSun], analytical pdf, analytical cdf
    """
    # Global parameter :
    # alpha: high mass power law exponent
    # beta: lowm ass power law exponent
    # mu: scale parameter (nearly the peak of the distribution)
    alpha = 2.3
    beta = 1.4
    mu = mu.value_in(u.MSun)

    mass_min = mass_min.value_in(u.MSun)
    mass_max = mass_max.value_in(u.MSun)

    # Define an auxiliary function G
    G = lambda m: ( 1 + (m/mu)**(1-alpha) )**(1-beta)

    # Cumulative distribution function L3-IMF :
    cdfL3 = lambda m: ( G(m) - G(mass_min) ) / ( G(mass_max) - G(mass_min))
    # Density distribution function
    # normalisation
    A = (1-alpha)*(1-beta)/mu / ( G(mass_max) - G(mass_min) )
    pdfL3 = lambda m: A*(m/mu)**(-alpha) * ( 1+ (m/mu)**(1-alpha) )**(-beta)

    # The inverse of the cumulative function is known in analytic form :
    # this allows to generate n masses directly from a list of random deviates
    invPl3 = lambda m: mu * ( (G(mass_min) + (G(mass_max)-G(mass_min))*m )**(1./(1-beta)) -1)**(1./(1-alpha))
    n = np.random.random(N)
    mass_distribution = invPl3(n)

    return mass_distribution | u.MSun, pdfL3, cdfL3


def log_simple(stars, E0=None):
    """
    Print energy log and compute energy errors
    :param stars: particle set to compute energy on
    :param converter: if stars is in SI units, to convert to nbody system
    :return: total energy, energy drift and total energy drift for next step
    """

    Ep = stars.potential_energy(G=nbo.G)
    Ek = stars.kinetic_energy()
    Et = Ep + Ek
    Q = Ek / -Ep
    dE = (Et - E0) / (np.abs(E0.number)|nbo.energy) if E0 is not None else 0
    print("Etot: {:.4f}, Ep: {:.4f}, Ek: {:.4f}".format(Et.value_in(nbo.energy),
                                                         Ep.value_in(nbo.energy),
                                                         Ek.value_in(nbo.energy),
                                                         ))
    print("dE: {:.3e}  |  Qvir: {:.4f}".format(dE, Q))
    return Et, Q, dE, Ek, Ep


def E_drift(energy):
    """
    Plot energy drift of a simulation
    :param energy: dictionnary where time, Etot and dE of a simulation are stored
                   as np.array
    :return: Figure and axe
    """
    fig, ax = plt.subplots()
    ax.plot(energy['time'], energy['dE'])
    ax.plot(energy['time'], energy['Etot'] - np.mean(energy['Etot']))
    ax.set_xlabel('time [nbody units]')
    ax.set_ylabel('Energy variations [nbody units]')
    ax.legend(['$dE = \dfrac{E_i - E_{i-1}}{E_{i-1}}$',
               '$E_{mean}$' + ' = {}'.format(np.mean(energy['Etot']))])

    return fig, ax 













