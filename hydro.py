# coding: utf8
"""

"""
import numpy as np
import matplotlib.pyplot as plt

from amuse.io import read_set_from_file, write_set_to_file
from amuse.datamodel import Particles, new_regular_grid
from amuse.ic.gasplummer import new_plummer_gas_model

from amuse.community.fi.interface import Fi

from amuse.units import quantities, constants, nbody_system, units
from amuse.support.console import set_printing_strategy

set_printing_strategy("custom", preferred_units=[units.MSun, units.parsec, units.Myr],
                      precision=4, prefix="", separator="[", suffix="]")

from help_functions import *

if __name__ == "__main__":

    # Initialise a gas cloud as a plummer sphere
    Ngas = 1000
    converter = nbody_system.nbody_to_si(500|units.MSun, 1 | units.pc)
    cloud = new_plummer_gas_model(Ngas, convert_nbody=converter)

    # Initialise SPH code
    sph = Fi(converter)
    sph.gas_particles.add_particles(cloud)

    sph.parameters.gas_epsilon = 0.01 | units.pc
    sph.parameters.eps_is_h_flag = True
    sph.parameters.gamma = 5./3  # default value: 1.6666667
    sph.parameters.isothermal_flag = False
    sph.parameters.integrate_entropy_flag = True
    # sph.parameters.opening_angle = 0.5
    # sph.parameters.n_smooth = 64
    # sph.parameters.courant = 0.3
    # sph.parameters.stopping_condition_maximum_density = 1e-16 | units.g / units.cm ** 3
    sph.parameters.timestep = cloud.dynamical_timescale()/500

    # Set communication
    to_cloud = sph.gas_particles.new_channel_to(cloud, ['density', 'h_smooth',
                                                        'mass', 'u',
                                                        'x', 'y', 'z',
                                                        'vx', 'vy', 'vz'])

    # plot a slice at the center of the cloud
    view=[-1., 1., -1., 1., 0, 0] | units.pc
    grid_size=(64, 64, 1)
    rho, v, cells_length = sph_to_grid(sph, view, grid_size)
    rho = rho.sum(axis=2).value_in(units.amu/units.cm**3)
    # rho = (rho.sum(axis=2) * cells_length[2]).value_in(units.amu / units.cm ** 2)
    ax = plot_hydro(rho, view[:4])

    # Save energies
    Ek, Ep, Eth = log_gas(sph)
    E0 = Ek + Ep + Eth

    # Evolve
    time = np.arange(0.2, 5, 0.2) | units.Myr
    for t in time:
        sph.evolve_model(t)
        to_cloud.copy()
        rho, v, cells_length = sph_to_grid(sph, view, grid_size)
        rho = rho.sum(axis=2).value_in(units.amu/units.cm**3)
        # rho = (rho.sum(axis=2) * cells_length[2]).value_in(units.amu / units.cm ** 2)
        _ = plot_hydro(rho, view[:4], ax=ax)
        plt.draw()
        plt.pause(.01)

        Ek, Ep, Eth = log_gas(sph, E0)
        E0 = Ek + Ep + Eth

    sph.stop()


    ############################################################################
    #########           Add stars to an SPH simulation         #################
    ############################################################################
    # stars = Particles(3)
    # stars.mass = [1, 1, 1] | units.MSun
    # stars.x = [-3**0.5/4, 0, 3**0.5/4] | units.pc
    # stars.y = [-1/4, 1/2, -1/4] | units.pc
    # stars.z = [0, 0, 0] | units.pc
    # stars.vx = np.random.uniform(-0.05, 0.05, 3) | units.kms
    # stars.vy = np.random.uniform(-0.05, 0.05, 3) | units.kms
    # stars.vz = [0, 0, 0] | units.kms

    # sph.star_particles.add_particles(stars)
    # to_stars = sph.star_particles.new_channel_to(stars)



